package eu.diversit.test.server;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.auth.DigestScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import static eu.diversit.test.server.MockHttpsServerBuilder.simpleHandler;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MockHttpsJettyServerTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(MockHttpsJettyServerTest.class);
    private MockHttpsServer httpsServer;

    public MockHttpsJettyServerTest() {
        this.httpsServer = new JettyServer(1080, 1443);
    }

//    @BeforeMethod
    public void startMockHttpsServer() throws Exception {
        httpsServer.start();
        LOGGER.info("Started "+httpsServer.getClass());
    }

//    @AfterSuite
    public void stopMockHttpsServer() throws Exception {
        httpsServer.stop();
        LOGGER.info("Stopped "+httpsServer.getClass());
    }

    @Test
    public void getContentUsingHttpClientWithDigest() throws Exception {

        MockHttpsServer2 server = new MockHttpsServerBuilder()
                .withHttpOn(1080)
                .withHttpsOn(1443)
                .withContextHandler(simpleHandler("Hello World"))
                    .onPath("/")
                .withBasicAuthentication()
                    .onPath("/*")
                    .addRole("admin")
                    .withRealmName("realm")
                    .addUserWithName("user1")
                    .andPassword("")
                    .andRole("")
                    .withHandler(null)
                .withDigestAuthentication()
                    .onPath("/*")
                    .addRole("admin")
                    .withRealmName("realm")
                    .addUserWithName("user1")
                    .andPassword("")
                    .andRole("")
                    .withHandler(null)
                .start();
        server.stop();


        DefaultHttpClient httpClient = null;
        try {
            SSLSocketFactory sslSocketFactory = new SSLSocketFactory(httpsServer.getSslContext());
            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("https", 1443, sslSocketFactory));
            PoolingClientConnectionManager mgr = new PoolingClientConnectionManager(registry);

            httpClient = new DefaultHttpClient(mgr);

            HttpHost targetHost = new HttpHost("localhost", 1443);
            httpClient.getCredentialsProvider().setCredentials(
                    new AuthScope(targetHost),
                    new UsernamePasswordCredentials("user2", "pass2")
            );

            DigestScheme digestScheme = new DigestScheme();
            digestScheme.overrideParamter("realm", "digestRealm");

            AuthCache authCache = new BasicAuthCache();
            authCache.put(targetHost, digestScheme);

            BasicHttpContext context = new BasicHttpContext();
            context.setAttribute(ClientContext.AUTH_CACHE, authCache);

            HttpGet get = new HttpGet("https://localhost:1443/sec/");
            HttpResponse response = httpClient.execute(get, context);

            String content = EntityUtils.toString(response.getEntity());

            LOGGER.debug(content);
            assertThat(content, is("Secure\n"));
        } finally {
            if (httpClient != null)
                httpClient.getConnectionManager().shutdown();
        }
    }

    @Test
    public void getContentUsingHttpClient() throws Exception {

        HttpClient httpClient = null;
        try {
            SSLSocketFactory sslSocketFactory = new SSLSocketFactory(httpsServer.getSslContext());
            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("https", 1443, sslSocketFactory));
            PoolingClientConnectionManager mgr = new PoolingClientConnectionManager(registry);

            httpClient = new DefaultHttpClient(mgr);
            HttpGet get = new HttpGet("https://localhost:1443");
            HttpResponse response = httpClient.execute(get);
            String content = EntityUtils.toString(response.getEntity());

            LOGGER.debug(content);
            assertThat(content, is("Hello\n"));
        } finally {
            if (httpClient != null)
                httpClient.getConnectionManager().shutdown();
        }
    }

    @Test
    public void getContentFromHttpsResource() throws Exception {

        String url = "https://localhost:1443";
        HttpsURLConnection urlConnection = (HttpsURLConnection) new URL(url).openConnection();
        urlConnection.setSSLSocketFactory(httpsServer.getSslContext().getSocketFactory());
        String content = getContent(urlConnection.getInputStream());

        LOGGER.debug(content);
        assertThat(content, is("Hello\n"));
    }

    @Test

    public void getContentFromHttpResource() throws IOException {

        String url = "http://localhost:1080";
        URLConnection urlConnection = new URL(url).openConnection();
        String content = getContent(urlConnection.getInputStream());

        LOGGER.debug(content);
        assertThat(content, is("Hello\n"));
    }

    private String getContent(InputStream inputStream) throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        StringBuilder content = new StringBuilder();

        String line;
        while((line = bufferedReader.readLine()) != null) {
            content.append(line).append("\n");
        }

        bufferedReader.close();
        inputStreamReader.close();
        return content.toString();
    }
}
