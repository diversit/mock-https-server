package eu.diversit.test.server

import org.slf4j.LoggerFactory
import javax.net.ssl.{TrustManagerFactory, KeyManagerFactory, SSLContext}
import java.security.KeyStore
import org.eclipse.jetty.server.{Connector, Server}
import org.eclipse.jetty.server.handler.{ContextHandlerCollection, ContextHandler, AbstractHandler}
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}
import org.eclipse.jetty.server.nio.SelectChannelConnector
import org.eclipse.jetty.util.ssl.SslContextFactory
import org.eclipse.jetty.server.ssl.SslSelectChannelConnector

abstract class MockHttpsServer2(httpPort:Int, httpsPort:Int,
                                 chc:HandlerConfig,
                                 acs:List[AuthenticationConfig]) {

  val Logger = LoggerFactory.getLogger("MockHttpsServer2")

  var sslContext:SSLContext = null

  def getSslContext() = {
    if(sslContext == null) {
      sslContext = initSslContext()
    }
    sslContext
  }

  def start():Unit
  def stop():Unit

  private def initSslContext() = {

    // Maken key store met self-signed certificate (in 'src/test/resources'):
    // keytool -genkey -v -alias mockhttps -dname "CN=localhost,OU=bol,O=com,c=NL" -keypass keypass -keystore mockhttps.jks -storepass storepass -keyalg "RSA" -sigalg "MD5withRSA" -keysize 2048 -validity 3650
    val keyStore = KeyStore.getInstance("JKS");
    val keyStoreResource = getClass().getClassLoader().getResourceAsStream("mockhttps.jks");
    require(keyStoreResource != null)

    keyStore.load(keyStoreResource, "storepass".toCharArray());

    printAliases(keyStore);

    val kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
    kmf.init(keyStore, "keypass".toCharArray());

    val sslContext = SSLContext.getInstance("SSLv3");
    val tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
    tmf.init(keyStore);

    sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
    sslContext;
  }

  def printAliases(keyStore:KeyStore):Unit = {

    val aliases = keyStore.aliases();

    while (aliases.hasMoreElements()) {
      val alias = aliases.nextElement();
      Logger.info(" - alias: " + alias);
      val certificate = keyStore.getCertificate(alias);
      Logger.info(" - cert type: " + certificate.getType());
      Logger.info(" - algorithm: " + certificate.getPublicKey().getAlgorithm());
      Logger.info(" - format: " + certificate.getPublicKey().getFormat());
    }
  }
}

class JettyServer2(httpPort:Int, httpsPort:Int,
                   chc:HandlerConfig,
                   acs:List[AuthenticationConfig])
  extends MockHttpsServer2(httpPort, httpsPort, chc, acs) {

  override val Logger = LoggerFactory.getLogger("JettyServer.class")

  val server:Server = new Server()

  class SimpleHandler(text:String) extends AbstractHandler {
    def handle(target:String, baseRequest:org.eclipse.jetty.server.Request, request:HttpServletRequest , response:HttpServletResponse) {
      response.setContentType("text/plain")
      response.setStatus(HttpServletResponse.SC_OK)
      baseRequest.setHandled(true)
      response.getWriter().println(text)
    }
  }

  def start() {
    var connectors = List[Connector]()

    if (httpPort > 0) {
      val httpConnector = new SelectChannelConnector()
      httpConnector.setPort(httpPort)
      connectors = connectors :+ httpConnector
    }

    if (httpsPort > 0) {
      val sslContext = getSslContext()
      val sslContextFactory = new SslContextFactory()
      sslContextFactory.setSslContext(sslContext)
      val httpsConnector = new SslSelectChannelConnector(sslContextFactory)
      httpsConnector.setPort(httpsPort)
      connectors = connectors :+ httpsConnector
    }
    server.setConnectors(connectors.toArray)

    var contextList = List[ContextHandler]()

    if (chc != null) {
      val rootContext = new ContextHandler()
      rootContext.setHandler(chc.handler)
      rootContext.setContextPath(chc.path)
      contextList = contextList :+ rootContext
    }

//    ContextHandler basicContext = new ContextHandler();
//    basicContext.setHandler(basicContextHandler());
//    basicContext.setContextPath("/basic");
//
//    ContextHandler secContext = new ContextHandler();
//    secContext.setHandler(digestContextHandler());
//    secContext.setContextPath("/sec");

    val contexts = new ContextHandlerCollection()
    contexts.setHandlers(contextList.toArray)
    server.setHandler(contexts)

    server.start()
  }

//  private SecurityHandler digestContextHandler() {
//    HashLoginService l = new HashLoginService();
//    l.putUser("user2", new Password("pass2"), new String[]{"admin"});
//    l.setName("digestRealm");
//
//    Constraint constraint = new Constraint();
//    constraint.setName(Constraint.__DIGEST_AUTH);
//    constraint.setRoles(new String[]{"admin"});
//    constraint.setAuthenticate(true);
//
//    ConstraintMapping cm = new ConstraintMapping();
//    cm.setConstraint(constraint);
//    cm.setPathSpec("/*");
//
//    ConstraintSecurityHandler csh = new ConstraintSecurityHandler();
//    csh.setAuthenticator(new DigestAuthenticator());
//    csh.setRealmName("myDigestRealm");
//    csh.addConstraintMapping(cm);
//    csh.setLoginService(l);
//    //        csh.setStrict(true);
//    csh.setHandler(new SimpleHandler("Secure"));
//
//    return csh;
//  }
//
//  private SecurityHandler basicContextHandler() {
//    HashLoginService l = new HashLoginService();
//    l.putUser("user1", Credential.getCredential("pass1"), new String[] {"user"});
//    l.setName("basicRealm");
//
//    Constraint constraint = new Constraint();
//    constraint.setName(Constraint.__BASIC_AUTH);
//    constraint.setRoles(new String[]{"user"});
//    constraint.setAuthenticate(true);
//
//    ConstraintMapping cm = new ConstraintMapping();
//    cm.setConstraint(constraint);
//    cm.setPathSpec("/*");
//
//    ConstraintSecurityHandler csh = new ConstraintSecurityHandler();
//    csh.setAuthenticator(new BasicAuthenticator());
//    csh.setRealmName("myrealm");
//    csh.addConstraintMapping(cm);
//    csh.setLoginService(l);
//    csh.setHandler(new SimpleHandler("Basic"));
//
//    return csh;
//  }

  def stop() {
    server.stop();
  }
}