package eu.diversit.test.server

import org.eclipse.jetty.server.handler.{AbstractHandler, ContextHandler}
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}
import eu.diversit.test.server.MockHttpsServerBuilder.SimpleHandler
import org.eclipse.jetty.server.{Handler, Request}


//////////////// BUILDER ///////////////////////////
case class MockHttpsServerBuilder(val httpPort:Int, val httpsPort:Int,
                                  val chc:HandlerConfig = null,
                                  val acs:List[AuthenticationConfig] = List()) {
  def this() = this(-1,-1)

  def withHttpOn(httpPort:Int) = new MockHttpsServerBuilder(httpPort, httpsPort)

  def withHttpsOn(httpsPort:Int) = new MockHttpsServerBuilder(httpPort, httpsPort)

  def withContextHandler(handler:Handler) = new HandlerBuilder(this, handler)

  def withBasicAuthentication = new BasicAuthenticationBuilder(this)

  def withDigestAuthentication = new DigestAuthenticationBuilder(this)

  def start:MockHttpsServer2 = {
    val server: MockHttpsServer2 = new JettyServer2(httpPort, httpsPort, chc, acs)
    server.start()
    server
  }
}

object MockHttpsServerBuilder {

  def apply = new MockHttpsServerBuilder()

  def simpleHandler(text:String) = new SimpleHandler(text)

  class SimpleHandler(text:String) extends AbstractHandler {
    def handle(target: String, baseRequest: Request, request: HttpServletRequest, response: HttpServletResponse): Unit = {
      response.setContentType("text/plain")
      response.setStatus(HttpServletResponse.SC_OK)
      baseRequest.setHandled(true)
      response.getWriter.println(text)
    }
  }
}

/////////////////// USER //////////////////////////////
case class User(name:String, pwd:String, role:String)

////////////////// AUTHENTICATION /////////////////////
class AuthenticationConfig(path:String, roles:List[String],
                            realm:String, users:List[User],
                            handler:ContextHandler)

abstract class AuthenticationBuilder[A <: AuthenticationConfig](val rootBuilder:MockHttpsServerBuilder, val path:String,
                                 val roles:List[String], val realm:String, val users:List[User]) {

  def createBuilder(rootBuilder:MockHttpsServerBuilder, path:String, roles:List[String], realm:String, users:List[User]):AuthenticationBuilder[A]

  def createConfig(path:String, roles:List[String], realm:String, users:List[User], handler:ContextHandler):A

  class BACRole(bab:AuthenticationBuilder[A]) {
    def addRole(role:String) = new BACRealm(createBuilder(bab.rootBuilder, bab.path, role :: bab.roles, bab.realm, bab.users))
  }
  class BACRealm(bab:AuthenticationBuilder[A]) {
    def withRealmName(realm:String) = new BACAddUser(createBuilder(bab.rootBuilder, bab.path, bab.roles, realm, bab.users))
  }
  class BACAddUser(bab:AuthenticationBuilder[A]) {
    def addUserWithName(name:String) = new BACUserBuilder(createBuilder(bab.rootBuilder, bab.path, bab.roles, realm, bab.users), name)
  }
  class BACUserBuilder(bab:AuthenticationBuilder[A], name:String, pwd:String="") {
    class BACUBRole(ub:BACUserBuilder) {
      def andRole(role:String):BACHandler = new BACHandler(createBuilder(bab.rootBuilder, bab.path, bab.roles, bab.realm, new User(name,pwd,role) :: bab.users))
    }
    def andPassword(pwd:String) = new BACUBRole(new BACUserBuilder(bab, name, pwd))
  }
  class BACHandler(bab:AuthenticationBuilder[A]) {
    def withHandler(handler:ContextHandler):MockHttpsServerBuilder = new MockHttpsServerBuilder(bab.rootBuilder.httpPort,
          bab.rootBuilder.httpsPort, bab.rootBuilder.chc,
          createConfig(bab.path, bab.roles, bab.realm, bab.users, handler) :: bab.rootBuilder.acs)
  }

  def onPath(path:String) = new BACRole(createBuilder(rootBuilder, path, roles, realm, users))
}

////////////////// BASIC AUTHENTICATION /////////////////////
class BasicAuthenticationConfig(path:String, roles:List[String],
                                realm:String, users:List[User],
                                handler:ContextHandler) extends AuthenticationConfig(path, roles, realm, users, handler)

class BasicAuthenticationBuilder(rootBuilder:MockHttpsServerBuilder, path:String = "",
roles:List[String] = List(), realm:String = "basic",
users:List[User] = List()) extends AuthenticationBuilder[BasicAuthenticationConfig](rootBuilder, path, roles, realm, users) {

  def createBuilder(rootBuilder:MockHttpsServerBuilder, path:String, roles:List[String], realm:String, users:List[User]) =
  new BasicAuthenticationBuilder(rootBuilder, path, roles, realm, users)

  def createConfig(path: String, roles: List[String], realm: String, users: List[User], handler: ContextHandler) =
  new BasicAuthenticationConfig(path, roles, realm, users, handler)
}

////////////////// DIGEST AUTHENTICATION /////////////////////
class DigestAuthenticationConfig(path:String, roles:List[String],
                                 realm:String, users:List[User],
                                 handler:ContextHandler) extends AuthenticationConfig(path, roles, realm, users, handler)

class DigestAuthenticationBuilder(rootBuilder:MockHttpsServerBuilder, path:String = "",
                                 roles:List[String] = List(), realm:String = "digest",
                                 users:List[User] = List()) extends AuthenticationBuilder[DigestAuthenticationConfig](rootBuilder, path, roles, realm, users) {

  def createBuilder(rootBuilder:MockHttpsServerBuilder, path:String, roles:List[String], realm:String, users:List[User]) =
    new DigestAuthenticationBuilder(rootBuilder, path, roles, realm, users)

  def createConfig(path: String, roles: List[String], realm: String, users: List[User], handler: ContextHandler) =
    new DigestAuthenticationConfig(path, roles, realm, users, handler)
}


case class HandlerConfig(val handler:Handler, val path:String)
case class HandlerBuilder(val serverBuilder:MockHttpsServerBuilder, val handler:Handler, val path:String = "/") {

  def onPath(path:String) = new MockHttpsServerBuilder(serverBuilder.httpPort, serverBuilder.httpsPort, new HandlerConfig(handler, path))

}

