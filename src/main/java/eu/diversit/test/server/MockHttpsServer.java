package eu.diversit.test.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.Certificate;
import java.util.Enumeration;

abstract public class MockHttpsServer {

    private static final Logger LOGGER = LoggerFactory.getLogger(MockHttpsServer.class);
    private SSLContext sslContext = null;

    int httpsPort;
    int httpPort;

    public MockHttpsServer(int httpPort, int httpsPort) {
        this.httpPort = httpPort;
        this.httpsPort = httpsPort;
    }

    public abstract void start() throws Exception;

    public abstract void stop() throws Exception;

    public SSLContext getSslContext() throws Exception {
        if(sslContext == null) {
            sslContext = initSslContext();
        }
        return sslContext;
    }

    private SSLContext initSslContext() throws Exception {

        // Maken key store met self-signed certificate (in 'src/test/resources'):
        // keytool -genkey -v -alias mockhttps -dname "CN=localhost,OU=bol,O=com,c=NL" -keypass keypass -keystore mockhttps.jks -storepass storepass -keyalg "RSA" -sigalg "MD5withRSA" -keysize 2048 -validity 3650
        KeyStore keyStore = KeyStore.getInstance("JKS");
        InputStream keyStoreResource = getClass().getClassLoader().getResourceAsStream("mockhttps.jks");
        assert keyStoreResource != null;

        keyStore.load(keyStoreResource, "storepass".toCharArray());

        printAliases(keyStore);

        KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        kmf.init(keyStore, "keypass".toCharArray());

        SSLContext sslContext = SSLContext.getInstance("SSLv3");
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(keyStore);

        sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
        return sslContext;
    }

    private void printAliases(KeyStore keyStore) throws KeyStoreException {
        Enumeration<String> aliases = keyStore.aliases();

        while (aliases.hasMoreElements()) {
            String alias = aliases.nextElement();
            LOGGER.info(" - alias: " + alias);
            Certificate certificate = keyStore.getCertificate(alias);
            LOGGER.info(" - cert type: " + certificate.getType());
            LOGGER.info(" - algorithm: " + certificate.getPublicKey().getAlgorithm());
            LOGGER.info(" - format: " + certificate.getPublicKey().getFormat());
        }
    }
}
