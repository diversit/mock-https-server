package eu.diversit.test.server;

import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.HashLoginService;
import org.eclipse.jetty.security.SecurityHandler;
import org.eclipse.jetty.security.authentication.BasicAuthenticator;
import org.eclipse.jetty.security.authentication.DigestAuthenticator;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.eclipse.jetty.server.ssl.SslSelectChannelConnector;
import org.eclipse.jetty.util.security.Constraint;
import org.eclipse.jetty.util.security.Credential;
import org.eclipse.jetty.util.security.Password;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JettyServer extends MockHttpsServer {

    private static final Logger LOGGER = LoggerFactory.getLogger(JettyServer.class);

    private Server server;

    public JettyServer(int httpPort, int httpsPort) {
        super(httpPort, httpsPort);
    }

    public static void main(String[] args) throws Exception {
        JettyServer jettyServer = new JettyServer(1080, 1443);
        jettyServer.start();
        jettyServer.server.join();
    }

    class SimpleHandler extends AbstractHandler {
        private String text;

        SimpleHandler(String text) {
            this.text = text;
        }

        public void handle(String target, org.eclipse.jetty.server.Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
            response.setContentType("text/plain");
            response.setStatus(HttpServletResponse.SC_OK);
            baseRequest.setHandled(true);
            response.getWriter().println(text);
        }
    }

    public void start() throws Exception {
        server = new Server();

        SelectChannelConnector httpConnector = new SelectChannelConnector();
        httpConnector.setPort(httpPort);
        SSLContext sslContext = getSslContext();
        SslContextFactory sslContextFactory = new SslContextFactory();
        sslContextFactory.setSslContext(sslContext);
        SslSelectChannelConnector httpsConnector = new SslSelectChannelConnector(sslContextFactory);
        httpsConnector.setPort(httpsPort);
        server.setConnectors(new Connector[] { httpConnector, httpsConnector });

        ContextHandler rootContext = new ContextHandler();
        rootContext.setHandler(new SimpleHandler("Hello"));
        rootContext.setContextPath("/");

        ContextHandler basicContext = new ContextHandler();
        basicContext.setHandler(basicContextHandler());
        basicContext.setContextPath("/basic");

        ContextHandler secContext = new ContextHandler();
        secContext.setHandler(digestContextHandler());
        secContext.setContextPath("/sec");

        ContextHandlerCollection contexts = new ContextHandlerCollection();
        contexts.setHandlers(new Handler[] {rootContext, secContext, basicContext});
        server.setHandler(contexts);

        server.start();
    }

    private SecurityHandler digestContextHandler() {
        HashLoginService l = new HashLoginService();
        l.putUser("user2", new Password("pass2"), new String[]{"admin"});
        l.setName("digestRealm");

        Constraint constraint = new Constraint();
        constraint.setName(Constraint.__DIGEST_AUTH);
        constraint.setRoles(new String[]{"admin"});
        constraint.setAuthenticate(true);

        ConstraintMapping cm = new ConstraintMapping();
        cm.setConstraint(constraint);
        cm.setPathSpec("/*");

        ConstraintSecurityHandler csh = new ConstraintSecurityHandler();
        csh.setAuthenticator(new DigestAuthenticator());
        csh.setRealmName("myDigestRealm");
        csh.addConstraintMapping(cm);
        csh.setLoginService(l);
//        csh.setStrict(true);
        csh.setHandler(new SimpleHandler("Secure"));

        return csh;
    }

    private SecurityHandler basicContextHandler() {
        HashLoginService l = new HashLoginService();
        l.putUser("user1", Credential.getCredential("pass1"), new String[] {"user"});
        l.setName("basicRealm");

        Constraint constraint = new Constraint();
        constraint.setName(Constraint.__BASIC_AUTH);
        constraint.setRoles(new String[]{"user"});
        constraint.setAuthenticate(true);

        ConstraintMapping cm = new ConstraintMapping();
        cm.setConstraint(constraint);
        cm.setPathSpec("/*");

        ConstraintSecurityHandler csh = new ConstraintSecurityHandler();
        csh.setAuthenticator(new BasicAuthenticator());
        csh.setRealmName("myrealm");
        csh.addConstraintMapping(cm);
        csh.setLoginService(l);
        csh.setHandler(new SimpleHandler("Basic"));

        return csh;
    }

    public void stop() throws Exception {
        server.stop();
    }
}