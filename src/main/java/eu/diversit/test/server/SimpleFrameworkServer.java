package eu.diversit.test.server;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;
import org.simpleframework.http.core.Container;
import org.simpleframework.http.core.ContainerServer;
import org.simpleframework.transport.connect.Connection;
import org.simpleframework.transport.connect.SocketConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintStream;
import java.net.InetSocketAddress;

public class SimpleFrameworkServer extends MockHttpsServer {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleFrameworkServer.class);

    private Connection connection;

    public SimpleFrameworkServer(int httpPort, int httpsPort) {
        super(httpPort, httpsPort);
    }

    public void start() throws Exception {
        Container container = new Container() {
            public void handle(Request request, Response response) {
                try {
                    response.set("Content-Type", "text/plain");
                    PrintStream body = response.getPrintStream();
                    body.println("Hello");
                    body.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        ContainerServer server = new ContainerServer(container);
        connection = new SocketConnection(server);
        connection.connect(new InetSocketAddress(httpsPort), getSslContext());
        connection.connect(new InetSocketAddress(httpPort));
    }

    public void stop() throws IOException {
        if (connection != null)
            connection.close();
    }
}
