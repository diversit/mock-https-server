package httpsmock;

import httpsmock.JettyServer.JettyServerBuilder.DigestContext;
import org.apache.commons.lang.StringUtils;
import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.HashLoginService;
import org.eclipse.jetty.security.authentication.DigestAuthenticator;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.eclipse.jetty.server.ssl.SslSelectChannelConnector;
import org.eclipse.jetty.util.security.Constraint;
import org.eclipse.jetty.util.security.Password;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.hamcrest.Matcher;

import javax.net.ssl.SSLContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.MatcherAssert.assertThat;

public class JettyServer extends MockHttpsServer {
    private Server server;
    private final List<DigestContext> contexts;
    private Handler rootHandler;

    public boolean hasRootHandler() {
        return rootHandler != null;
    }
    public Handler getRootHandler() {
        return rootHandler;
    }

    public static void main(String[] args) throws Exception {
        JettyServer jettyServer1 = JettyServerBuilder.newServer()
                .onHttpPort(1080)
                .onHttpsPort(1443)
                .withRootHandler(new SimpleHandler("Defautl root handler"))
                .addDigestContext()
                    .withRealm("Builder test")
                    .withHandler(new SimpleHandler("It worked!"))
                    .allowUser("boller", "pwd", "admin")
                    .onContextPath("/sec2")
                .create();
        jettyServer1.start();
        jettyServer1.server.join();
    }

    public JettyServer(int httpPort, int httpsPort) {
        this(httpPort, httpsPort, new ArrayList<DigestContext>(), null);
    }

    public JettyServer(int httpPort, int httpsPort, List<DigestContext> contexts, Handler rootHandler) {
        super(httpPort, httpsPort);
        this.contexts = contexts;
        this.rootHandler = rootHandler;
    }

    public static class JettyServerBuilder {
        private int httpPort;
        private int httpsPort;
        private List<DigestContext> contexts;
        private Handler rootHandler;

        private JettyServerBuilder() {
            contexts = newArrayList();
        }

        public static JettyServerBuilder newServer() {
            return new JettyServerBuilder();
        }
        public JettyServerBuilder onHttpPort(int httpPort) {
            this.httpPort = httpPort;
            return this;
        }
        public JettyServerBuilder onHttpsPort(int httpsPort) {
            this.httpsPort = httpsPort;
            return this;
        }
        public JettyServerBuilder withRootHandler(Handler handler) {
            this.rootHandler = handler;
            return this;
        }
        public DigestContext addDigestContext() {
            DigestContext newContext = new DigestContext(this);
            contexts.add(newContext);
            return newContext;
        }
        public JettyServer create() {
            return new JettyServer(httpPort, httpsPort, contexts, rootHandler);
        }

        public class DigestContext {
            private JettyServerBuilder builder;
            private String realm;
            private List<User> allowedUsers;
            private Handler handler;
            private String contextPath;

            public DigestContext(JettyServerBuilder serverBuilder) {
                builder = serverBuilder;
                allowedUsers = newArrayList();
            }
            public DigestContext withRealm(String realm) {
                this.realm = realm;
                return this;
            }
            public DigestContext withHandler(Handler handler) {
                this.handler = handler;
                return this;
            }
            public DigestContext allowUser(String username, String pwd, String role) {
                allowedUsers.add(new User(username, pwd, role));
                return this;
            }
            public JettyServerBuilder onContextPath(String path) {
                this.contextPath = path;
                return builder;
            }

            public String getContextPath() {
                return contextPath;
            }

            public Handler digestContextHandler() {
                HashLoginService l = new HashLoginService();

                for(User user : allowedUsers) {
                    l.putUser(user.username, new Password(user.password), new String[]{ user.role });
                }
                l.setName(realm);

                Constraint constraint = new Constraint();
                constraint.setName(Constraint.__DIGEST_AUTH);
                constraint.setRoles(new String[]{"admin"});
                constraint.setAuthenticate(true);

                ConstraintMapping cm = new ConstraintMapping();
                cm.setConstraint(constraint);
                cm.setPathSpec("/*");

                ConstraintSecurityHandler csh = new ConstraintSecurityHandler();
                csh.setAuthenticator(new DigestAuthenticator());
                csh.setRealmName(realm);
                csh.addConstraintMapping(cm);
                csh.setLoginService(l);
//              csh.setStrict(true);
                csh.setHandler(handler);

                return csh;
            }
        }

        public class User {
            private final String username;
            private final String password;
            private final String role;

            public User(String username, String password, String role) {
                this.username = username;
                this.password = password;
                this.role = role;
            }
        }
    }

    public static class MockHandlerBuilder<T> {

        private String responseContentType;
        private String responseString;
        private int responseStatus;
        private Matcher<T> sendDataMatcher;
        private Class<T> requestType;
        private CountDownLatch countDownLatch;
        private String path = "/";

        private MockHandlerBuilder() { }

        public static <T> MockHandlerBuilder newHandler() {
            return new MockHandlerBuilder();
        }

        public MockHandlerBuilder matchSendData(Class<T> requestType, Matcher<T> matcher) {
            this.requestType = requestType;
            this.sendDataMatcher = matcher;
            return this;
        }

        public MockHandlerBuilder setResponseContentType(String responseContentType) {
            this.responseContentType = responseContentType;
            return this;
        }

        public MockHandlerBuilder setResponse(String responseString){
            this.responseString = responseString;
            return this;
        }

        public MockHandlerBuilder setResponseStatus(int responseStatus) {
            this.responseStatus = responseStatus;
            return this;
        }

        public MockHandlerBuilder setPath(String handlerPath) {
            this.path = handlerPath;
            return this;
        }

        public MockHandler build() {
            return new MockHandler();
        }

        public MockHandlerBuilder countDown(CountDownLatch latch) {
            countDownLatch = latch;
            return this;
        }

        public class MockHandler extends AbstractHandler {

            public void handle(String s, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
                baseRequest.setHandled(true);
                assertThatDataReceivedIsEqualToDataSend(request);

                setContentType(response);
                setResponseStatus(response);
                setResponseContent(response);
                countDown();
            }

            public String getPath() {
                return path;
            }

            private void countDown() {
                if(countDownLatch != null) {
                    countDownLatch.countDown();
                }
            }

            private void setResponseContent(HttpServletResponse response) throws IOException {
                if(StringUtils.isNotBlank(responseString)) {
                    response.getWriter().println(responseString);
                }
            }

            private void setResponseStatus(HttpServletResponse response) {
                if(responseStatus > 0) {
                    response.setStatus(responseStatus);
                }
            }

            private void setContentType(HttpServletResponse response) {
                if(StringUtils.isNotBlank(responseContentType)) {
                    response.setContentType(responseContentType);
                }
            }

            private void assertThatDataReceivedIsEqualToDataSend(HttpServletRequest request) throws IOException, ServletException {
                try {
                    assertThat(unmarshallXmlFromRequest(request), sendDataMatcher);
                } catch (JAXBException e) {
                    throw new ServletException(e);
                }
            }

            private T unmarshallXmlFromRequest(HttpServletRequest request) throws JAXBException, IOException {
                Unmarshaller unmarshaller = JAXBContext.newInstance(requestType).createUnmarshaller();
                T unmarshal = (T) unmarshaller.unmarshal(request.getInputStream());
                return unmarshal;
            }
        }
    }


    public static class SimpleHandler extends AbstractHandler {
        private String text;

        public SimpleHandler(String text) {
            this.text = text;
        }

        public void handle(String target, org.eclipse.jetty.server.Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
            response.setContentType("text/plain");
            response.setStatus(HttpServletResponse.SC_OK);
            baseRequest.setHandled(true);
            response.getWriter().println(text);
        }
    }

    public void start() {
        server = new Server();

        List<Connector> connectors = newArrayList();
        if(httpPort > 0) {
            SelectChannelConnector httpConnector = new SelectChannelConnector();
            httpConnector.setPort(httpPort);
            connectors.add(httpConnector);
        }
        if(httpsPort > 0) {
            SSLContext sslContext = getSslContext();
            SslContextFactory sslContextFactory = new SslContextFactory();
            sslContextFactory.setSslContext(sslContext);
            SslSelectChannelConnector httpsConnector = new SslSelectChannelConnector(sslContextFactory);
            httpsConnector.setPort(httpsPort);
            connectors.add(httpsConnector);
        }
        server.setConnectors(connectors.toArray(new Connector[connectors.size()]));

        ContextHandlerCollection contextHandlers = new ContextHandlerCollection();
        if(contexts.isEmpty() || hasRootHandler()) {
            // add a default context
            ContextHandler baseHandler = new ContextHandler();
            baseHandler.setHandler(hasRootHandler() ? getRootHandler() : getDefaultHandler());
            baseHandler.setContextPath("/");
            contextHandlers.addHandler(baseHandler);
        }
        if(!contexts.isEmpty()) {
            for(DigestContext context : contexts) {
                ContextHandler contextHandler = new ContextHandler();
                contextHandler.setHandler(context.digestContextHandler());
                contextHandler.setContextPath(context.getContextPath());
                contextHandlers.addHandler(contextHandler);
            }
        }

        server.setHandler(contextHandlers);

        try {
            server.start();
        } catch (Exception e) {
            throw new RuntimeException("Error starting JettyServer", e);
        }
    }

    private SimpleHandler getDefaultHandler() {
        return new SimpleHandler("Jetty running on port " + httpPort + " and " + httpsPort + " ...");
    }

    public void stop() {
        try {
            server.stop();
        } catch (Exception e) {
            throw new RuntimeException("Error stopping JettyServer", e);
        }
    }

    public URL getServerURL() {
        String scheme = server.getConnectors()[0].getConfidentialScheme();
        String hostnameAndPort = server.getConnectors()[0].getName();
        String url = String.format("%s://%s", scheme, hostnameAndPort);
        url = url.replace("0.0.0.0","localhost");
        try {
            return new URL(url);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
}