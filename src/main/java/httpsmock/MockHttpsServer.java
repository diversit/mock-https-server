package httpsmock;

import httpsmock.JettyServer.JettyServerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.InputStream;
import java.net.URL;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.Certificate;
import java.util.Enumeration;

abstract public class MockHttpsServer {

    private static final Logger LOGGER = LoggerFactory.getLogger(MockHttpsServer.class);
    public static final String PASSWORD_STORE = "storepass";
    public static final String PASSWORD_KEY = "keypass";
    private SSLContext sslContext = null;

    int httpsPort;
    int httpPort;

    public static JettyServerBuilder newServer() {
        return JettyServerBuilder.newServer();
    }

    public MockHttpsServer(int httpPort, int httpsPort) {
        this.httpPort = httpPort;
        this.httpsPort = httpsPort;
    }

    public abstract void start();

    public abstract void stop();

    public abstract URL getServerURL();

    public SSLContext getSslContext() {
        if(sslContext == null) {
            sslContext = initSslContext();
        }
        return sslContext;
    }

    private SSLContext initSslContext() {

        // Maken key store met self-signed certificate (in 'src/test/resources'):
        // keytool -genkey -v -alias mockhttps -dname "CN=localhost,OU=bol,O=com,c=NL" -keypass keypass -keystore keystore.jks -storepass storepass -keyalg "RSA" -sigalg "MD5withRSA" -keysize 2048 -validity 3650
        try {
            KeyStore keyStore = KeyStore.getInstance("JKS");
            InputStream keyStoreResource = getClass().getClassLoader().getResourceAsStream("httpsmock/keystore.jks");
            assert keyStoreResource != null;

            keyStore.load(keyStoreResource, PASSWORD_STORE.toCharArray());

            printAliases(keyStore);

            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(keyStore, PASSWORD_KEY.toCharArray());

            SSLContext sslContext = SSLContext.getInstance("SSLv3");
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(keyStore);

            sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
            return sslContext;
        } catch (Exception e) {
            throw new RuntimeException("Error initializing SSLContext", e);
        }
    }

    private void printAliases(KeyStore keyStore) throws KeyStoreException {
        Enumeration<String> aliases = keyStore.aliases();

        while (aliases.hasMoreElements()) {
            String alias = aliases.nextElement();
            LOGGER.info(" - alias: " + alias);
            Certificate certificate = keyStore.getCertificate(alias);
            LOGGER.info(" - cert type: " + certificate.getType());
            LOGGER.info(" - algorithm: " + certificate.getPublicKey().getAlgorithm());
            LOGGER.info(" - format: " + certificate.getPublicKey().getFormat());
        }
    }
}
